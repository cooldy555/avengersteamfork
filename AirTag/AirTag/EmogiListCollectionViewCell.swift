//
//  EmogiListCollectionViewCell.swift
//  AirTag
//
//  Created by Cristian Cosneanu on 6/13/17.
//  Copyright © 2017 Cristian Cosneanu. All rights reserved.
//

import UIKit

class EmogiListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var emogiImage: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
}
